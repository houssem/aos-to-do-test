<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\User;

class ToDo extends Model
{
    protected $table = "to_do";
    protected $fillable = ['id', 'name', 'description', 'created_at', 'updated_ad', 'status', 'user_id'];
    public $timestamps = true;

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
