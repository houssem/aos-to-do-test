<?php

namespace App\Http\Controllers;

use App\Entities\ToDo;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ToDoController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:onlyOwnerCan,toDo', ['except' => [
            'add', 'show', 'index'
        ]]);
    }

    /**
     * Display tasks.
     * @param GET
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function index()
    {
        try {
            return ToDo::orderBy('created_at', 'DESC')
                ->get();
        } catch (Exception $e) {
            Log::info($e->getMessage());
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }

    /**
     * insert task.
     * @method POST
     * @param Illuminate\Http\Request
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function add(Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);
            $data['user_id'] = Auth::user()->id;
            $toDo = ToDo::create($data);
            return response()->json(
                [
                    'error' => false,
                    'message' => 'Task added successfully!'
                ],
                200
            );
        } catch (Exception $e) {
            Log::info(sprintf("Error while adding task, with message: %s", $e->getMessage()));
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }

    /**
     * update task.
     * @method PUT
     * @param Illuminate\Http\Request
     * @param ToDo $toDo
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function edit(ToDo $toDo, Request $request)
    {
        try {
            $data = json_decode($request->getContent(), true);
            if ($toDo->update($data)) {
                return response()->json(
                    [
                        'error' => false,
                        'message' => 'Task updated successfully!'
                    ],
                    200
                );
            }
            return response()->json(
                [
                    'error' => true,
                    'message' => 'Error while updating task'
                ],
                200
            );
        } catch (Exception $e) {
            Log::info(sprintf("Error while updating task, with message: %s", $e->getMessage()));
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }
    /**
     * Delete task.
     * @method DELETE
     * @param ToDo $toDo
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function delete(ToDo $toDo)
    {
        try {
            if ($toDo->delete()) {
                return response()->json(
                    [
                        'error' => false,
                        'message' => 'Task deleted successfully!'
                    ],
                    200
                );
            }
            return response()->json(
                [
                    'error' => true,
                    'message' => 'Error while deleting task'
                ],
                200
            );
        } catch (Exception $e) {
            Log::info(sprintf("Error while deleting task, with message: %s", $e->getMessage()));
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }

    /**
     * Update task status to complete.
     * @method GET
     * @param ToDo $toDo
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function taskCompleted(ToDo $toDo)
    {
        try {
            $toDo->status = 'complete';
            if ($toDo->save()) {
                return response()->json(
                    [
                        'error' => false,
                        'message' => 'Task Completed!'
                    ],
                    200
                );
            }
            return response()->json(
                [
                    'error' => true,
                    'message' => 'Error while updating task status'
                ],
                200
            );
        } catch (Exception $e) {
            Log::info(sprintf("Error while updating task status, with message: %s", $e->getMessage()));
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }

    /**
     * Update task status to not complete.
     * @method GET
     * @param ToDo $toDo
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function taskNotCompleted(ToDo $toDo)
    {
        try {
            $toDo->status = 'not_complete';
            if ($toDo->save()) {
                return response()->json(
                    [
                        'error' => false,
                        'message' => 'Task Not Completed!'
                    ],
                    200
                );
            }
            return response()->json(
                [
                    'error' => true,
                    'message' => 'Error while updating task status'
                ],
                200
            );
        } catch (Exception $e) {
            Log::info(sprintf("Error while updating task status, with message: %s", $e->getMessage()));
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }

    /**
     * show task details.
     * @method GET
     * @param ToDo $toDo
     * @return JsonResponse
     * @author Selmi Houssem <selmi_houssem@hotmil.fr>
     */
    public function showForEdit(ToDo $toDo)
    {
        try {
            return $toDo;
        } catch (Exception $e) {
            Log::info(sprintf("Error while getting task , with message: %s", $e->getMessage()));
            return response()->json(
                [
                    'error' => true,
                    'message' => $e->getMessage() . "There's an error"
                ],
                500
            );
        }
    }
}
