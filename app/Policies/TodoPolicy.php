<?php

namespace App\Policies;

use App\Entities\ToDo;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TodoPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can delete the to do.
     *
     * @param  \App\User  $user
     * @param  \App\ToDo  $toDo
     * @return mixed
     */
    public function onlyOwnerCan(User $user, ToDo $toDo)
    {
        return $user->id === $toDo->user_id;
    }
}
