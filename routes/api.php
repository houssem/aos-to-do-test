<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(
    ['middleware' => 'auth:api'],
    function () {
        Route::post('/task/add', 'ToDoController@add');
        Route::put('/task/edit/{toDo}', 'ToDoController@edit');
        Route::delete('/task/delete/{toDo}', 'ToDoController@delete');
        Route::get('/task/completed/{toDo}', 'ToDoController@taskCompleted');
        Route::get('/task/notcompleted/{toDo}', 'ToDoController@taskNotCompleted');
        Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
        Route::get('/tasks', 'ToDoController@index');
        Route::get('/task/showForEdit/{toDo}', 'ToDoController@showForEdit');
    }
);
Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
Route::post('/register', 'Auth\ApiAuthController@register')->name('register.api');
